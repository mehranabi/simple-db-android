package abi.mehran.database.utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import abi.mehran.database.Category;

public class DatabaseHelper extends SQLiteOpenHelper {
    public DatabaseHelper(Context context) {
        super(context, "shop", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE categories (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(50))");
        db.execSQL("INSERT INTO categories (name) VALUES ('Phone')");
        db.execSQL("INSERT INTO categories (name) VALUES ('Laptop')");
        db.execSQL("INSERT INTO categories (name) VALUES ('Camera')");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
    }

    public List<Category> getCategories() {
        List<Category> categories = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM categories;", new String[] {});
        cursor.moveToFirst();

        if (cursor.getCount() <= 0) {
            return categories;
        }

        do {
            Category category = new Category(
                    cursor.getInt(cursor.getColumnIndexOrThrow("id")),
                    cursor.getString(cursor.getColumnIndexOrThrow("name"))
            );
            categories.add(category);
        } while(cursor.moveToNext());

        return categories;
    }
}
