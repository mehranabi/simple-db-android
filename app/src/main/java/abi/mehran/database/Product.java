package abi.mehran.database;

public class Product {
    public int id;
    public int categoryId;
    public String name;

    public Product(int id, int categoryId, String name) {
        this.id = id;
        this.categoryId = categoryId;
        this.name = name;
    }
}
