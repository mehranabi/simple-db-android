package abi.mehran.database;

import androidx.annotation.NonNull;

public class Category {
    public int id;
    public String name;

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @NonNull
    @Override
    public String toString() {
        return this.name;
    }
}
