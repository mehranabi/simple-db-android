package abi.mehran.database;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import abi.mehran.database.utils.DatabaseHelper;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DatabaseHelper database = new DatabaseHelper(this);
        List<Category> categories = database.getCategories();

        ListView listView = findViewById(R.id.list);
        ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(this, R.layout.list_item, categories);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Category category = categories.get(i);
                Toast.makeText(MainActivity.this, "Clicked on " + category.name, Toast.LENGTH_SHORT).show();
            }
        });
    }
}